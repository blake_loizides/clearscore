//
//  ClearScoreTests.swift
//  ClearScoreTests
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import XCTest
@testable import ClearScore

class ClearScoreTests: XCTestCase {
  func testCreditScoreViewController() {
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    guard let vc = storyboard.instantiateViewController(identifier: "CreditScoreViewController") as? CreditScoreViewController else {
      return
    }
    let expectation = self.expectation(description: "creditScore")
    vc.viewModel.getCreditScore(success: { (creditScore) in
      XCTAssertNotNil(creditScore)
      XCTAssertNotNil(creditScore?.creditReportInfo?.score)
      XCTAssertNotNil(creditScore?.creditReportInfo?.maxScoreValue)
      XCTAssertLessThan(vc.viewModel.getActualMaxCreditScore().0, vc.viewModel.getActualMaxCreditScore().1)
      vc.creditScoreProgressview?.setProgress(fromProgress: 0, creditScore: vc.viewModel.getActualMaxCreditScore().0, maxCreditScore: vc.viewModel.getActualMaxCreditScore().1, animate: true)
      expectation.fulfill()
    }) { (error) in
      XCTAssert(false, error)
      expectation.fulfill()
    }
    waitForExpectations(timeout: 10, handler: nil)
  }
}
