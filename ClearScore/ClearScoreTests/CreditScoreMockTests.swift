//
//  CreditScoreMock.swift
//  ClearScoreTests
//
//  Created by Blake Loizides on 3/8/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import XCTest
import ObjectMapper
@testable import ClearScore

class CreditScoreMockTests: XCTestCase {
  func testCreditModelDictionary() {
    let mockModelDictionary = ["accountIDVStatus":"PASS","dashboardStatus":"PASS","personaType":"INEXPERIENCED", "creditReportInfo":["score":514,"scoreBand":4,"clientRef":"CS-SED-655426-708782","status":"MATCH","maxScoreValue":700,"minScoreValue":0,"monthsSinceLastDefaulted":-1,"hasEverDefaulted":false,"monthsSinceLastDelinquent":1,"hasEverBeenDelinquent":true,"percentageCreditUsed":44,"percentageCreditUsedDirectionFlag":1,"changedScore":0,"currentShortTermDebt":13758,"currentShortTermNonPromotionalDebt":13758,"currentShortTermCreditLimit":30600,"currentShortTermCreditUtilisation":44,"changeInShortTermDebt":549,"currentLongTermDebt":24682,"currentLongTermNonPromotionalDebt":24682,"currentLongTermCreditLimit":"null","currentLongTermCreditUtilisation":"null","changeInLongTermDebt":-327,"numPositiveScoreFactors":9,"numNegativeScoreFactors":0,"equifaxScoreBand":4,"equifaxScoreBandDescription":"Excellent","daysUntilNextReport":9]] as [String : Any]
    let creditScore = Mapper<CreditScore>().map(JSON: mockModelDictionary)
    XCTAssertEqual(creditScore?.creditReportInfo?.score,514)
    XCTAssertEqual(creditScore?.creditReportInfo?.maxScoreValue,700)
    guard let score = creditScore?.creditReportInfo?.score, let maxScoreValue = creditScore?.creditReportInfo?.maxScoreValue else {
      return XCTAssert(false)
    }
    XCTAssertLessThan(score, maxScoreValue)
  }
}
