//
//  UiColorExtension.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/7/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import Foundation

import UIKit

extension UIColor {
  static let baseYellow = UIColor().returnSafeColor(colorName: "baseYellow")
  
  fileprivate func returnSafeColor(colorName:String) -> UIColor {
    let color = UIColor(named:colorName)
    guard let returnColor = color else {
      return color ?? UIColor()
    }
    return returnColor
  }
}
