//
//  ActivityIndicatorView.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/8/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import UIKit

extension UIViewController {
  
  var activityIndicatorTag: Int {
    return 10000
  }
  
  internal func startActivityIndicator(style: UIActivityIndicatorView.Style = UIActivityIndicatorView.Style.large, location: CGPoint? = nil) {
    let centerLocation = location ?? self.view.center
    DispatchQueue.main.async {
      let activityIndicator = UIActivityIndicatorView(style: style)
      activityIndicator.tag = self.activityIndicatorTag
      activityIndicator.center = centerLocation
      activityIndicator.hidesWhenStopped = true
      activityIndicator.startAnimating()
      self.view.addSubview(activityIndicator)
    }
  }
  
  internal func stopActivityIndicator() {
    DispatchQueue.main.async {
      if let activityIndicator = self.view.subviews.filter({ $0.tag == self.activityIndicatorTag}).first as? UIActivityIndicatorView {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
      }
    }
  }
}

extension UIViewController {
  
  internal func showAlertView(title:String, message:String) {
    let alertController = UIAlertController(title:title,
                                            message: message,
                                            preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK",
                                 style: .default,
                                 handler: nil)
    alertController.addAction(OKAction)
    self.present(alertController,
                 animated: true,
                 completion: nil)
  }
}
