//
//  StringExtensions.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/7/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import UIKit

extension NSAttributedString {
  func setCreditScoreAtrributedLabel(actualCreditScore:String, text:String) -> NSAttributedString {
    let creditScoreAttributedString = NSMutableAttributedString(string: text)
    let actualCreditScoreAttributes = [NSAttributedString.Key.foregroundColor:UIColor.baseYellow, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 30, weight: .regular)]
    creditScoreAttributedString.addAttributes(actualCreditScoreAttributes, range: (text as NSString).range(of:actualCreditScore))
    return creditScoreAttributedString
  }
}

