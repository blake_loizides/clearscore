//
//  CreditReportinfo.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import Foundation
import ObjectMapper

internal struct CreditReportInfo: Mappable {
  internal var score: Int?
  internal var scoreBand: Int?
  internal var clientRef: String?
  internal var status: String?
  internal var maxScoreValue: Int?
  internal var minScoreValue: Int?
  internal var monthsSinceLastDefaulted: Int?
  internal var hasEverDefaulted: Bool?
  internal var monthsSinceLastDelinquent: Int?
  internal var hasEverBeenDelinquent: Bool?
  internal var percentageCreditUsed: Int?
  internal var percentageCreditUsedDirectionFlag: Int?
  internal var changedScore: Int?
  internal var currentShortTermDebt: Int?
  internal var currentShortTermNonPromotionalDebt: Int?
  internal var currentShortTermCreditLimit: Int?
  internal var currentShortTermCreditUtilisation: Int?
  internal var changeInShortTermDebt: Int?
  internal var currentLongTermDebt: Int?
  internal var currentLongTermNonPromotionalDebt: Int?
  internal var currentLongTermCreditLimit: Int?
  internal var currentLongTermCreditUtilisation: Int?
  internal var changeInLongTermDebt: Int?
  internal var numPositiveScoreFactors: Int?
  internal var numNegativeScoreFactors: Int?
  internal var equifaxScoreBand: Int?
  internal var equifaxScoreBandDescription: String?
  internal var daysUntilNextReport: Int?

  init?(map: Map) {
  }
  
  mutating internal func mapping(map: Map) {
    score <- map["score"]
    scoreBand <- map["scoreBand"]
    clientRef <- map["clientRef"]
    status <- map["status"]
    maxScoreValue <- map["maxScoreValue"]
    minScoreValue <- map["minScoreValue"]
    monthsSinceLastDefaulted <- map["monthsSinceLastDefaulted"]
    hasEverDefaulted <- map["hasEverDefaulted"]
    monthsSinceLastDelinquent <- map["monthsSinceLastDelinquent"]
    hasEverBeenDelinquent <- map["hasEverBeenDelinquent"]
    percentageCreditUsed <- map["percentageCreditUsed"]
    changedScore <- map["changedScore"]
    currentShortTermDebt <- map["currentShortTermDebt"]
    currentShortTermNonPromotionalDebt <- map["currentShortTermNonPromotionalDebt"]
    currentShortTermCreditLimit <- map["currentShortTermCreditLimit"]
    currentShortTermCreditUtilisation <- map["currentShortTermCreditUtilisation"]
    changeInShortTermDebt <- map["changeInShortTermDebt"]
    currentLongTermDebt <- map["currentLongTermDebt"]
    currentLongTermNonPromotionalDebt <- map["currentLongTermNonPromotionalDebt"]
    currentLongTermCreditLimit <- map["currentLongTermCreditLimit"]
    currentLongTermCreditUtilisation <- map["currentLongTermCreditUtilisation"]
    changeInLongTermDebt <- map["changeInLongTermDebt"]
    numPositiveScoreFactors <- map["numPositiveScoreFactors"]
    numNegativeScoreFactors <- map["numNegativeScoreFactors"]
    equifaxScoreBand <- map["equifaxScoreBand"]
    equifaxScoreBandDescription <- map["equifaxScoreBandDescription"]
    daysUntilNextReport <- map["daysUntilNextReport"]
  }
}



