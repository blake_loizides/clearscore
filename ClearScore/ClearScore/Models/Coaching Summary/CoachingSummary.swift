//
//  CoachingSummary.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import Foundation
import ObjectMapper

internal struct CoachingSummary: Mappable {
  internal var activeTodo: Bool?
  internal var activeChat: Bool?
  internal var numberOfTodoItems: Int?
  internal var numberOfCompletedTodoItems: Int?
  internal var selected: Bool?

  init?(map: Map) {
  }
  
  mutating internal func mapping(map: Map) {
    activeTodo <- map["activeTodo"]
    activeChat <- map["activeChat"]
    numberOfTodoItems <- map["numberOfTodoItems"]
    numberOfCompletedTodoItems <- map["numberOfCompletedTodoItems"]
    selected <- map["selected"]
    }
}
