//
//  CreditScore.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import Foundation
import ObjectMapper

internal struct CreditScore: Mappable {
  internal var accountIdvStatus: String?
  internal var creditReportInfo: CreditReportInfo?
  internal var dashboardStatus: String?
  internal var personaType: String?
  internal var augmentedCreditScore: Int?
  internal var coachingSummary: CoachingSummary?

  init?(map: Map) {
  }
  
  mutating internal func mapping(map: Map) {
    accountIdvStatus <- map["accountIDVStatus"]
    creditReportInfo <- map["creditReportInfo"]
    dashboardStatus <- map["dashboardStatus"]
    personaType <- map["personaType"]
    augmentedCreditScore <- map["augmentedCreditScore"]
    coachingSummary <- map["coachingSummary"]
    }
}
