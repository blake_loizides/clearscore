//
//  SoftSensorError.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import Foundation
import ObjectMapper

internal struct ApiError: Mappable {
  var message: String?

  init?(map: Map) {
  }
  
  mutating internal func mapping(map: Map) {
    message <- map["message"]
  }
}
