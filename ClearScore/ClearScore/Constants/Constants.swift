//
//  Constants.swift
//  SnappCars
//
//  Created by Blake Loizides on 8/15/18.
//  Copyright © 2018 BCLab. All rights reserved.
//

import Foundation
import UIKit

struct ApiConstants {
  struct Server {
    static let baseUrl = "https://5lfoiyb0b3.execute-api.us-west-2.amazonaws.com/prod/mockcredit/"
  }
}

struct SslPinningConstants {
  struct Host {
    static let baseUrl = "5lfoiyb0b3.execute-api.us-west-2.amazonaws.com"
  }
}
