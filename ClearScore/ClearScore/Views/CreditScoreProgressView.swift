//
//  CreditScoreDoughnutView.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import UIKit

class CreditScoreDoughnutView: UIView {
  
  fileprivate let progressBorderLayerPadding: CGFloat = 8.0
  
  fileprivate let trackLayer: CAShapeLayer = {
    let trackLayer = CAShapeLayer()
    trackLayer.fillColor = UIColor.clear.cgColor
    trackLayer.strokeStart = 0.0
    return trackLayer
  }()
  
  fileprivate let creditScoreProgressLayer: CAShapeLayer = {
    let progressLayer = CAShapeLayer()
    progressLayer.fillColor = UIColor.clear.cgColor
    progressLayer.strokeColor = UIColor.blue.cgColor
    progressLayer.lineCap = CAShapeLayerLineCap.round
    progressLayer.strokeStart = 0.0
    progressLayer.strokeEnd = 0.0
    return progressLayer
  }()
  
  fileprivate let progressBorderLayer: CAShapeLayer = {
    let progressBorderLayer = CAShapeLayer()
    progressBorderLayer.fillColor = UIColor.clear.cgColor
    progressBorderLayer.strokeStart = 0.0
    progressBorderLayer.strokeColor = UIColor.black.cgColor
    progressBorderLayer.lineWidth = 1.0
    progressBorderLayer.isHidden = true
    return progressBorderLayer
  }()
  
  fileprivate let creditScoreLabel: UILabel = {
    let creditScoreLabel = UILabel()
    creditScoreLabel.numberOfLines = 3
    creditScoreLabel.textAlignment = .center
    creditScoreLabel.textColor = UIColor.darkGray
    creditScoreLabel.font = UIFont.systemFont(ofSize: 18, weight: .regular)
    return creditScoreLabel
  }()
  
  internal var progress: CGFloat = 0 {
    didSet {
      creditScoreProgressLayer.strokeEnd = progress
    }
  }
  
  fileprivate var lineWidth: CGFloat = 20 {
    didSet {
      creditScoreProgressLayer.lineWidth = lineWidth
      trackLayer.lineWidth = lineWidth
    }
  }
  
  fileprivate var trackColor: UIColor = UIColor.lightGray {
    didSet {
      trackLayer.strokeColor = trackColor.cgColor
    }
  }
  
  fileprivate var progressColor: UIColor = UIColor.darkGray {
    didSet {
      creditScoreProgressLayer.strokeColor = progressColor.cgColor
    }
  }
  
  // MARK: - Init
  override init(frame: CGRect) {
    super.init(frame: frame)
    commonInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    commonInit()
  }
  
  fileprivate func commonInit() {
    layer.addSublayer(trackLayer)
    layer.addSublayer(creditScoreProgressLayer)
    layer.addSublayer(progressBorderLayer)
  }
  
  // MARK: - InterfaceBuilder
  override func prepareForInterfaceBuilder() {
    super.prepareForInterfaceBuilder()
    commonInit()
  }
  
  // MARK: - Viewlifecycle
  override func layoutSubviews() {
    super.layoutSubviews()
    let radius = (min(frame.size.width, frame.size.height) - lineWidth)/2
    let center = CGPoint(x: bounds.midX, y: bounds.midY)
    let startAngle = 1.5 * CGFloat.pi
    let endAngle = startAngle + 2 * CGFloat.pi
    let circlePath = UIBezierPath(arcCenter: center, radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
    trackLayer.path = circlePath.cgPath
    creditScoreProgressLayer.path = circlePath.cgPath
    
    let borderRadius = (min(frame.size.width+2, frame.size.height+2))/2
    let borderCirlcePath = UIBezierPath(arcCenter: center, radius: borderRadius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
    progressBorderLayer.path = borderCirlcePath.cgPath

    self.creditScoreLabel.frame = bounds
    self.creditScoreLabel.center = center
    self.progressColor = UIColor.baseYellow
    self.trackColor = UIColor.clear
    self.lineWidth = 5.0
    self.addSubview(self.creditScoreLabel)
  }
  
  // MARK: - Animate
  internal func setProgress(fromProgress:Int,
                            creditScore:Int,
                            maxCreditScore:Int,
                            animate:Bool) {
    let creditScoreDivision = Float(creditScore) / Float(maxCreditScore)
    guard animate else {
      creditScoreProgressLayer.strokeEnd = progress
      return
    }
    self.progress = CGFloat(creditScoreDivision)
    let animation = CABasicAnimation(keyPath: "strokeEnd")
    animation.duration = CFTimeInterval(2)
    animation.fromValue = Float(fromProgress)
    animation.toValue = Float(progress)
    animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
    self.progressBorderLayer.isHidden = false
    creditScoreProgressLayer.strokeEnd = progress
    creditScoreProgressLayer.add(animation, forKey: "animateDoughnut")
    creditScoreLabel.attributedText = NSAttributedString().setCreditScoreAtrributedLabel(actualCreditScore: String(creditScore), text: "Your credit score is \n\(creditScore) \nout of \(maxCreditScore)")
  }
}
