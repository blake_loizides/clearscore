//
//  CreditScoreViewController.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import UIKit

class CreditScoreViewController: UIViewController {
  
  @IBOutlet internal var creditScoreProgressview: CreditScoreDoughnutView?
  internal var viewModel = CreditScoreViewModel()
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.navigationController?.setNavigationBarHidden(true,
                                                      animated: animated)
    self.setupViewWillAppear()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.setupView()
  }
  
  fileprivate func setupViewWillAppear() {
    navigationItem.largeTitleDisplayMode = .automatic
  }
}

extension CreditScoreViewController {
  
  fileprivate func setupView() {
    DispatchQueue.main.async {
      self.startActivityIndicator()
      self.viewModel.getCreditScore(success: { (creditScore) in
        self.stopActivityIndicator()
        self.setProgress()
      }) { (errorMessage) in
        self.showAlertView(title: "Error", message: errorMessage)
        self.stopActivityIndicator()
      }
    }
  }
  
  fileprivate func setProgress() {
    self.creditScoreProgressview?.setProgress(fromProgress: 0, creditScore: self.viewModel.getActualMaxCreditScore().0, maxCreditScore: self.viewModel.getActualMaxCreditScore().1, animate: true)
  }
}
