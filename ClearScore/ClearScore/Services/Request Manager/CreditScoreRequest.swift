//
//  CreditScoreRequest.swift
//  ClearScore
//
//  Created by Blake Loizides on 8/1/19.
//  Copyright © 2018 BClab. All rights reserved.
//

import Foundation

extension RequestManager {
  struct CreditScoreRequest {
    internal func getCreditScore<T>(onSuccess:@escaping ((ApiResult<T>)->()),
                                onFailure:@escaping ((ApiError?)->())) {
      RequestManager.request(route:ApiRouter.CreditScoreRouter.getCreditScore,
                           responseHandler: ResponseHandler_CreditScoreResults.getCreditScore,
                             statusCodeRange: 200..<300,
                             onSuccess: { (result:ApiResult<T>) in
                              onSuccess(result)
      }) { (errors) in
        onFailure(errors)
      }
    }
  }
}
