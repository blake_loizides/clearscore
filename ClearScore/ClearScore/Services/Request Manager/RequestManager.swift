//
//  RequestManager.swift
//  ClearScore
//
//  Created by Blake Loizides on 8/1/19.
//  Copyright © 2018 BClab. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class RequestManager {
  
  fileprivate static let encodingKey = "accept"
  fileprivate static let sessionConfiguration = URLSessionConfiguration.default
  fileprivate static var requestManager = Session()

  static func configureAPIManager() {
    let serverTrustManager = ServerTrustManager(evaluators:
      [SslPinningConstants.Host.baseUrl: DisabledEvaluator()])
    let manager = Session(configuration: sessionConfiguration, serverTrustManager:serverTrustManager)
    requestManager = manager
  }

  static func request<T>(route:URLRequestConvertible, responseHandler:ResponseHandler,
                      statusCodeRange:Range<Int>,
                      onSuccess:@escaping ((ApiResult<T>)->()),
                      onFailure:@escaping ((ApiError?)->())) {
    requestManager.request(route).responseJSON { (response) in
      if let statusCode = response.response?.statusCode {
        if statusCodeRange.contains(statusCode) ||
          statusCodeRange.lowerBound == response.response?.statusCode {
          onSuccess(responseHandler.handleResponseData(response))
        } else {
          onFailure(responseHandler.handleErrors(response))
          return
        }
      }
    }
  }
}


