//
//  ResponseHandler.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.

import Foundation
import Alamofire

protocol ResponseHandler {
  func handleResponseData<T>(_ response:DataResponse<Any, AFError>) -> ApiResult<T>
  func handleErrors(_ response:DataResponse<Any, AFError>) -> ApiError?
}

extension DataResponse {
  func getValue() -> Success? {
    return self.value
  }
}
