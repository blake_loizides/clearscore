//
//  ResponseHandler_BluetoothScanResults.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.

import Foundation
import Alamofire
import ObjectMapper

enum ResponseHandler_CreditScoreResults: ResponseHandler {
  
  case getCreditScore
  
  var description: String {
    switch self {
    case .getCreditScore:
      return "getCreditScore"
    }
  }
  
  func handleResponseData<T>(_ response:DataResponse<Any, AFError>) -> ApiResult<T> {
    switch self {
    case .getCreditScore:
      return ApiResult(
        message: "Success",
        rawRequest: response.request,
        rawResponse: response.response,
        statusCode: response.response?.statusCode,
        value: extractResults(response))
    }
  }
  
  func handleErrors(_ response:DataResponse<Any, AFError>) -> ApiError? {
    guard let errorDict = response.getValue() as? [String: AnyObject] else {return nil}
    return Mapper<ApiError>().map(JSON: errorDict)
  }
  
  fileprivate func extractResults<T>(_ response:DataResponse<Any, AFError>) -> T? {
    var newResults: CreditScore?
    guard let dict = response.value as? [String:AnyObject] else {
      return newResults as? T
    }
    newResults = Mapper<CreditScore>().map(JSON: dict)
    return newResults as? T
  }
}
