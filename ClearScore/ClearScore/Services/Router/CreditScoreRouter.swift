//
//  CreditScoreRouter.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.


import Foundation
import Alamofire

extension ApiRouter {
  /**
   Encapsulates all the route contruction logic for the `UserRouter` endpoint.
   */
  enum CreditScoreRouter: URLRequestConvertible {
    
    case getCreditScore
    
    internal var path: String {
      switch self {
      case .getCreditScore:
        return "values"
      }
    }
    
    internal var method: HTTPMethod {
      switch self {
      default:
        return .get
      }
    }
    
    // MARK: - URLRequestConvertible
    internal func asURLRequest() throws -> URLRequest {
      let url = try ApiConstants.Server.baseUrl.asURL()
      var urlRequest = URLRequest(url: url.appendingPathComponent(path))
      urlRequest.httpMethod = method.rawValue
      return try URLEncoding.default.encode(urlRequest as URLRequestConvertible,
                                            with: nil)
    }
  }
}

