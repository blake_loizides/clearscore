//
//  CreditScoreViewModel.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import Foundation

internal class CreditScoreViewModel {
  
  fileprivate var creditScore: CreditScore?
  internal weak var delegate: CreditScoreViewDelegate?

  internal func getCreditScore(success: @escaping (CreditScore?) -> (),
                               failure: @escaping (String) -> ()) {
    RequestManager.CreditScoreRequest().getCreditScore(onSuccess: { (result: ApiResult<(CreditScore)>) in
      if let creditScore = result.value {
        self.creditScore = creditScore
        return success(creditScore)
      }
    }) { (error) in
      guard let errorMesssage = error?.message else {
        return
      }
      failure(errorMesssage)
    }
  }
  
  internal func getActualMaxCreditScore() -> (Int, Int) {
    guard let actualCreditScore = creditScore?.creditReportInfo?.score, let maxCreditScore = creditScore?.creditReportInfo?.maxScoreValue else {
      return (0, 0)
    }
    return (actualCreditScore,maxCreditScore)
  }
}
