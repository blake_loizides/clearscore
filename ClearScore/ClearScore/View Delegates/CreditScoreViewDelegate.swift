//
//  CreditScoreViewDelegate.swift
//  ClearScore
//
//  Created by Blake Loizides on 3/5/20.
//  Copyright © 2020 BCLab. All rights reserved.
//

import Foundation

// Note: I left this file in the project becuase I never used the protocol in this test project as I felt that becuase there was only a limited amount of code used, I went with a colsure based return.
protocol CreditScoreViewDelegate: class {
  func returnCreditScore(creditReport:CreditScore)
  func returnError(errorMessage:String)
}
